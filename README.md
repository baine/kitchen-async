# kitchen-async

```reason
let f = asyncPair((downloadConfig, readFile))

f((configUrl, filePath), (e, (config, fileContents)) => {
  Js.log2('config', config)
  Js.log2('file', fileContents)
})
```