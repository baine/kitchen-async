type timerId;
type nullable('a) = Js.Nullable.t('a);
type jsCallback('a) = (nullable(exn), nullable('a)) => unit;
type jsAsync('a) = jsCallback('a) => unit;
type callback('a) = Belt.Result.t('a, exn) => unit;
type async('a) = callback('a) => unit;
let toResult: (nullable(exn), nullable('a)) => Belt.Result.t('a, exn);

let asyncify: ('a => 'b, 'a) => async('b);
let jsAsyncify: ('a => 'b, 'a) => jsAsync('b);
let bsify: jsAsync('a) => async('a);
let onlyOnce: callback('a) => callback('a);
let asyncPair: ((async('a), async('b))) => async(('a, 'b));
let asyncTriplet:
  ((async('a), async('b), async('c))) => async(('a, 'b, 'c));
let asyncQuadruplet:
  ((async('a), async('b), async('c), async('d))) =>
  async(('a, 'b, 'c, 'd));
let asyncQuintuplet:
  ((async('a), async('b), async('c), async('d), async('e))) =>
  async(('a, 'b, 'c, 'd, 'e));
let asyncSextuplet:
  (
    (async('a), async('b), async('c), async('d), async('e), async('f))
  ) =>
  async(('a, 'b, 'c, 'd, 'e, 'f));

let asyncReduce: (('b, 'a) => async('b), 'b, array('a)) => async('b);
let delay: (int, 'a) => async('a);

let map: ('a => 'b, async('a)) => async('b);
let flatMap: ('a => async('b), async('a)) => async('b);
let sequenceArray: array(async('a)) => async(array('a));
