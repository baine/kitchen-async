open Belt.Result;
open! KitchenAsync;

let f = asyncify(x => x + 1);
let g = asyncify(x => "hello " ++ x);

let logger = (tag, x) =>
  switch (x) {
  | Error(e) => Js.log3(tag, "error", e)
  | Ok(r) => Js.log3(tag, "ok", r)
  };

asyncPair((f(7), g("andrew")), logger("pair"));

let laterDouble = n => delay(1000, 2 * n);
let laterDecrement = n => delay(1000, n - 1);
let laterIncrement = n => delay(1000, n + 1);

let laterSum = (b, a) => delay(1000, b + a);

let laterAppend = (b, a) => delay(1000, b ++ "-" ++ string_of_int(a));

sequenceArray(
  Belt.Array.map([|1, 2, 3, 4|], laterDouble),
  logger("sequenceArray"),
);

asyncReduce(
  laterSum,
  0,
  [|1, 2, 3, 4, 5, 6, 7, 8, 9, 10|],
  logger("asyncReduce-1"),
);

asyncReduce(
  laterAppend,
  "0",
  [|1, 2, 3, 4, 5, 6, 7, 8, 9, 10|],
  logger("asyncReduce-2"),
);

let rec xs = n =>
  switch (n) {
  | 0 => ""
  | n => "x" ++ xs(n - 1)
  };

let pipeline = x =>
  x
  |> laterIncrement
  |> flatMap(laterDouble)
  |> flatMap(laterDouble)
  |> flatMap(laterDouble)
  |> flatMap(laterDecrement)
  |> flatMap(asyncify(xs));

pipeline(1, logger("pipeline"));
