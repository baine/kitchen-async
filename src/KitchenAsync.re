open Belt.Result;

type timerId;
[@bs.val] external setTimeout: (unit => unit, int) => timerId = "";

exception Impossible;

type nullable('a) = Js.Nullable.t('a);
type err = exn;
type jsCallback('a) = (nullable(err), nullable('a)) => unit;
type jsAsync('a) = jsCallback('a) => unit;

type callback('a) = t('a, err) => unit;
type async('a) = callback('a) => unit;

let toResult = (e, r) =>
  switch (Js.Nullable.toOption(e), Js.Nullable.toOption(r)) {
  | (Some(e), _) => Error(e)
  | (_, Some(r)) => Ok(r)
  | _ => raise(Impossible)
  };

let flatMap = (g, f) => {
  let h = cb =>
    f(result =>
      switch (result) {
      | Error(_) as e => cb(e)
      | Ok(x) => g(x, cb)
      }
    );
  h;
};

/* takes a synchronous function and makes it async */
let asyncify: ('a => 'b, 'a) => async('b) =
  (f, x, cb) =>
    cb(
      try (Ok(f(x))) {
      | e => Error(e)
      },
    );

/* takes a synchronous function and makes it async */
let jsAsyncify: ('a => 'b, 'a) => jsAsync('b) =
  (f, x, cb) =>
    try (cb(Js.Nullable.null, Js.Nullable.return(f(x)))) {
    | e => cb(Js.Nullable.return(e), Js.Nullable.null)
    };

/* takes an async function and turns it into a bucklescript async function */
let bsify: jsAsync('a) => async('a) =
  f => {
    let g = cb => f((err, result) => cb(toResult(err, result)));
    g;
  };

let onlyOnce: callback('a) => callback('a) =
  cb => {
    let called = ref(false);
    r =>
      if (! called^) {
        called := true;
        cb(r);
      };
  };

/* takes a pair of functions and returns an async function of pairs */
let asyncPair: ((async('a), async('b))) => async(('a, 'b)) =
  ((f: async('a), g: async('b)), cb: callback(('p, 'q))) => {
    let cb = onlyOnce(cb);
    let p = ref(None);
    let q = ref(None);
    let handler = reference =>
      onlyOnce(r =>
        switch (r) {
        | Error(_) as e => cb(e)
        | Ok(r) =>
          reference := Some(r);
          switch (p^, q^) {
          | (Some(p), Some(q)) => cb(Ok((p, q)))
          | _ => ()
          };
        }
      );
    f(handler(p));
    g(handler(q));
  };

let asyncTriplet:
  ((async('a), async('b), async('c))) => async(('a, 'b, 'c)) =
  ((f, g, h), cb) =>
    asyncPair((f, asyncPair((g, h))), result =>
      switch (result) {
      | Error(_) as e => cb(e)
      | Ok((p, (q, r))) => cb(Ok((p, q, r)))
      }
    );

let asyncQuadruplet:
  ((async('a), async('b), async('c), async('d))) =>
  async(('a, 'b, 'c, 'd)) =
  ((f, g, h, i), cb) =>
    asyncPair((asyncPair((f, g)), asyncPair((h, i))), result =>
      switch (result) {
      | Error(_) as e => cb(e)
      | Ok(((p, q), (r, s))) => cb(Ok((p, q, r, s)))
      }
    );

let asyncQuintuplet = ((f, g, h, i, j), cb) =>
  asyncPair((asyncPair((f, g)), asyncTriplet((h, i, j))), result =>
    switch (result) {
    | Error(_) as e => cb(e)
    | Ok(((p, q), (r, s, t))) => cb(Ok((p, q, r, s, t)))
    }
  );

let asyncSextuplet = ((f, g, h, i, j, k), cb) =>
  asyncPair((asyncTriplet((f, g, h)), asyncTriplet((i, j, k))), result =>
    switch (result) {
    | Error(_) as e => cb(e)
    | Ok(((a, b, c), (d, e, f))) => cb(Ok((a, b, c, d, e, f)))
    }
  );

let sequenceArray = (fs, cb) =>
  switch (Belt.Array.length(fs)) {
  | 0 => cb(Ok([||]))
  | _ =>
    let cb = onlyOnce(cb);
    let ys = Belt.Array.map(fs, _ => None);
    let remaining = ref(Belt.Array.length(ys));

    let callback = i =>
      onlyOnce(result =>
        switch (result) {
        | Error(_) as e => cb(e)
        | Ok(y) =>
          Belt.Array.set(ys, i, Some(y)) |> ignore;
          remaining := remaining^ - 1;
          if (remaining^ == 0) {
            cb(Ok(Belt.Array.keepMap(ys, x => x)));
          };
        }
      );
    Belt.Array.forEachWithIndex(fs, (i, f) => f(callback(i)));
  };

let map: ('a => 'b, async('a)) => async('b) =
  (map, f, cb) =>
    f(r =>
      cb(
        switch (r) {
        | Error(_) as error => error
        | Ok(a) =>
          try (Ok(map(a))) {
          | e => Error(e)
          }
        },
      )
    );

let asyncReduce: (('b, 'a) => async('b), 'b, array('a)) => async('b) =
  (f: ('b, 'a) => async('b), b: 'b, xs: array('a), cb: callback('b)) => {
    let rec h = (i, b) =>
      if (i >= Belt.Array.length(xs)) {
        cb(Ok(b));
      } else {
        let a = Belt.Array.getUnsafe(xs, i);
        f(
          b,
          a,
          onlyOnce(result =>
            switch (result) {
            | Error(_) as e => cb(e)
            | Ok(b) => h(i + 1, b)
            }
          ),
        );
      };
    h(0, b);
  };

let delay = (d, x, cb) => setTimeout(() => cb(Ok(x)), d) |> ignore;
